// Enum and Struct Demo
// Benjamin Hohn

#include <iostream>
#include <conio.h>

enum Suit {

	HEART, CLUB, SPADE, DIAMOND
};

enum Rank {

	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

struct Card {

	Suit suit;
	Rank rank;
};

int main() {

	_getch();
	return 0;
}
